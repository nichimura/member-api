## Requirements

- [Node v7.6+](https://nodejs.org/en/download/current/) or [Docker](https://www.docker.com/)
- [Yarn](https://yarnpkg.com/en/docs/install)

## Getting Started

#### Install dependencies:

yarn

#### Set environment variables:

cp .env.example .env

## Running Locally

yarn dev

## Running in Production

yarn start

## Docker

# run container locally

yarn docker:dev

# run container in production

yarn docker:prod
